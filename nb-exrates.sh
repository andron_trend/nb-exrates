#! /bin/bash
# v. 0.2

## это будет "завтра"
date=`date -d "+1 day" "+%Y-%m-%d"`

####### БЛОК НАСТРОЕК #######
#
## временный файл для хранения результата ##
data=/tmp/nbrb-exrates.json
## лог-файл ##
logfile=/var/log/nb-exrates.log
## номера для SMS`ок и почта для оповещений
resp_phones=375296629688,375447356317
resp_mails="andron@bmz.gomel.by, kdm.asu@bmz.gomel.by"
## откуда берем данные
src_url="https://www.nbrb.by/api/exrates/rates?ondate=$date&periodicity=0"
## куда постим результат
dest_url="http://metdb.bsw.iron:8001/sap/bc/z_json_services/currency?sap-client=112"
## номер попытки запуска скрипта, по дефолту 1 ##
tries="${1:-1}"
## число попыток 
maxtries=5
#/////// БЛОК НАСТРОЕК ///////

## код возврата, по дефлту 0 (без ошибок) ##
rc=0

## весь вывод в журнал
exec >> $logfile

echo  ---- GET exrates at `date -Iminutes` to $date tries $tries ----
rm -f $data

# запрос данных с сервера НБРБ
/usr/bin/wget --no-cache -t 3 --timeout=5\
	      --output-document=$data \
	      -q $src_url 	      # 2>&1
rc=$(($rc+$?))
# размер полученного JSON
size=`stat -c %s $data`

if [[ $rc -ne 0 ]] || [[ size -lt 100 ]]; then
	tries=$(($tries+1))
 	if [[ tries -gt $maxtries ]]; then
		# Исчерпано число попыток, оповещаем о фатальной ошибке и завершаем работу
		# SMS`ка
		/usr/bin/wget \
			--output-document=/tmp/rc-curr.json \
			-q "http://fms/smsend.php?to=$resp_phones&msg=NB-Exrates GET ERR $rc"
		# письмо на email
		echo "NB ExRates GET error $rc" | mail -s NB-ExRates $resp_mails
		echo "FATAL error on GET, RC=$rc"
		exit
	fi
	# запланируем попытку через 15 минут и выходим
	echo "${0} $tries" | at now +15minute
	exit
fi

# отправка данных в SAP ERP
/usr/bin/wget --post-file=$data -q $dest_url 2>&1

rc=$(($rc+$?))
# если случилась ошибка
if [ $rc -ne 0 ]
then
	/usr/bin/wget \
        --output-document=/tmp/rc-curr.json \
        -q "http://fms/smsend.php?to=resp_phones&msg=NB-Exrates POST ERR $rc"
	
	echo "NB ExRates POST error $rc" | mail -s NB-ExRates $resp_mails
	echo POST $rc
	exit
fi

echo "rcode="$rc file size=$size
